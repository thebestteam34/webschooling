from django.apps import AppConfig


class authConfig(AppConfig):
    name = 'auth'
    label = 'auth2'