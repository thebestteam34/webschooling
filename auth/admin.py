from django.contrib import admin

from auth.models import Student
from django.contrib.auth.models import User
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.db.models import Q

from courses.models import Course


@admin.register(Student)
class StudentsAdmin(admin.ModelAdmin):
    list_display = ('user', 'birthday')

