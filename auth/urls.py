from django.conf.urls import include, url
from django.contrib import admin
import auth.views
import courses.views

urlpatterns = [
    url(r'login/', auth.views.logIn2, name="login"),
    url(r'logout/', auth.views.user_logout, name='logout'),
    url(r'register/', auth.views.register2, name="register"),
    url(r'user/(?P<userhash>-?\d+)', auth.views.confirmUser, name='confirm'),
    url(r'user/company$', auth.views.proposal_to_be_company, name='proposal_to_be_company'),
    url(r'user/profile$', auth.views.profile, name='profile'),
    url(r'user/profile/edit$', auth.views.profileEdit, name='edit'),
    url(r'user/proposal$', auth.views.proposal, name='proposal'),  # post_request
    url(r'user/proposal/(?P<hash>-?\d+)$', auth.views.confirmCompany, name='confirm_company'),
    url(r'user/proposal/(?P<hash>-?\d+)/delete$', auth.views.deleteCompany, name='delete_company'),
    url(r'user/unsubscribe$', auth.views.un_subscribe, name='un_teach'),
    url(r'certificates$',auth.views.show_certificates,name='certificates'),
]
