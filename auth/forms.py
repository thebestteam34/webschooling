import datetime
import hashlib

import cloudinary
import cloudinary.uploader
import cloudinary.api
from django import forms
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import AuthenticationForm, UsernameField
from django.contrib.auth.models import User
from django.core.mail import send_mail

from auth.models import Student
from courses.models import Company
from project.settings import SALT


class AuthForm(AuthenticationForm):
    username = UsernameField(
        label='Email',
        max_length=254,
        widget=forms.TextInput(attrs={'autofocus': ''}),
    )


class RegistrationFormUser(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name']

    city = forms.CharField(widget=forms.TextInput, required=False, label_suffix="(not required)")
    birthday = forms.DateField(widget=forms.SelectDateWidget(years=list(range(1918, 2018))),
                               initial=datetime.date.today(), required=False, label_suffix="(not required)")
    university = forms.CharField(widget=forms.TextInput, required=False, label_suffix="(not required)")
    password = forms.CharField(widget=forms.PasswordInput, label_suffix='*')
    password_again = forms.CharField(widget=forms.PasswordInput, label_suffix='*')

    def save(self, commit=True):
        if self.cleaned_data['password'] == self.cleaned_data['password_again']:
            user = User.objects.create_user(username=self.cleaned_data['email'],
                                            first_name=self.cleaned_data['first_name'],
                                            last_name=self.cleaned_data['last_name'],
                                            email='False')
            user.set_password(self.cleaned_data['password'])
            user.save()
            hash_form = hash(SALT + str(hash(user)))
            if hash_form < 0:
                hash_form = - hash_form
            student = Student(user=user)
            student.city = self.cleaned_data['city']
            student.birthday = self.cleaned_data['birthday']
            student.university = self.cleaned_data['university']
            student.student_hash = hash_form
            student.save()
            self.send_mail('auth/user/' + str(hash_form) + '?parameter=register')
            return user
        else:
            return None

    def send_mail(self, urlLink):
        subject = 'Proposal to sign up in EducationSphere.'
        message = 'Hi %s,Thanks for signing up ' \
                  'for EducationSphere! Please click this link to be register http://educationsp.herokuapp.com/%s ' \
                  'to confirm your email address\n. This means you will ' \
                  'be able to reset your password if ' \
                  'you forget it later, which is especially important if ' \
                  'you have a paid account!\n If you can\'t click the link ' \
                  'from your email program, please copy this URL and paste it ' \
                  'into your web browser. If you don\'t want to use PythonAnywhere,' \
                  ' just ignore this message and we won\'t bother you again.' \
                  % (self.cleaned_data['email'], urlLink)
        send_mail(subject, message, self.cleaned_data['email'], [self.cleaned_data['email']],
                  fail_silently=False)


class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company
        fields = ['name', 'about']

    def clean_name(self):
        if Company.objects.filter(name=self.cleaned_data['name']).exists():
            raise forms.ValidationError("This name is already in use")
        else:
            return self.cleaned_data['name']

    def is_valid_errors(self, request):
        if Company.objects.filter(name=self.cleaned_data['name']).exists():
            return 'Company with this name already exists'
        else:
            return None

    def sendmail(self, student, hash_code):
        subject = 'Proposal to be Company/Author in EduSphere'
        message = 'Hello %s' \
                  'Thanks for creating your company!\n' \
                  'Please https://educationsp.herokuapp.com/%s to confirm your email address ' \
                  'and send request to be self company.' \
                  ' \nThis means you will be able to reset your password if ' \
                  'you forget it later, which is especially important if ' \
                  'you have a paid account! \nIf you can\'t click the link ' \
                  'from your email program, please copy this URL and paste it ' \
                  'into your web browser: %s If you don\'t want to use PythonAnywhere,' \
                  ' just ignore this message and we won\'t bother you again. Cheers,' \
                  'The EducationSphere team' % (student.user.first_name, hash_code, hash_code)
        users = User.objects.filter(is_superuser=True)
        if users.exists():
            send_mail(subject, message, student.user.username, [student.user.username],
                      fail_silently=False)


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username']


class StudentForm(forms.ModelForm):
    class Meta():
        model = Student
        fields = ['city', 'university', 'birthday', 'photo']

    def saveForm(self, id):
        student = Student.objects.get(id=id)
        if 'photo' in self.changed_data:
            try:
                s = str(self.cleaned_data['photo']).split('.')
                e = str()
                for i in range(0, len(s) - 1):
                    e += s[i] + '.'
                filename = e[0:-1]
                print(filename)
                slash = filename.rfind("/")
                m = hashlib.md5()
                m.update(filename[slash + 1:].encode("UTF-8"))
                md5name = m.hexdigest()
                student.photo = self.cleaned_data['photo']
                student.photo.name = filename[0:slash + 1] + md5name + student.photo.name[
                                                                       student.photo.name.rfind("."):]
                student.save()
                cloudinary.uploader.upload(student.photo,
                                           public_id='media/' + student.photo.name[0:student.photo.name.rfind(".")])
            except:
                pass
        student.university = self.cleaned_data['university']
        student.birthday = self.cleaned_data['birthday']
        student.city = self.cleaned_data['city']
        student.save()
        return student
