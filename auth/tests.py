import time

from django.contrib.auth.models import User
from django.test import LiveServerTestCase, override_settings
from selenium import webdriver

from auth.models import Student


class CourseTests(LiveServerTestCase):
    def setUp(self):
        # User.objects.create_superuser(
        #     username='admin',
        #     password='admin',
        #     email='admin@example.com'
        # )
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_registration(self):
        self.register()


        print("=============")
        print(User.objects.all().first())

        users = User.objects.all()
        self.assertEqual(len(users), 1)

        students = Student.objects.all()
        self.assertEqual(len(students), 1)

    @override_settings(DEBUG=True)
    def test_authorization(self):
        self.register()
        self.authorize()
        self.assertEqual(self.browser.find_element_by_xpath("/html/body/div[1]/div/div[2]/div/ul/li[2]").text,
                         "UsernameFirst")

    # Registration
    def register(self):
        self.browser.get(
            '%s%s' % (self.live_server_url, "/auth/register/")
        )
        email = "timurgrunge@gmail.com"
        self.browser.find_element_by_id("id_email").send_keys(email)
        self.browser.find_element_by_id("id_first_name").send_keys("UsernameFirst")
        self.browser.find_element_by_id("id_last_name").send_keys("UsernameLast")
        self.browser.find_element_by_id("id_city").send_keys("City")
        self.browser.find_element_by_id("id_university").send_keys("University")
        self.browser.find_element_by_id("id_password").send_keys("123qwe")
        self.browser.find_element_by_id("id_password_again").send_keys("123qwe")
        self.browser.find_element_by_tag_name("form").submit()
        time.sleep(1)

        user = User.objects.all().first()
        self.browser.get(
            '%s%s' % (self.live_server_url, "/auth/user/" +
                      user.student_user.student_hash + "?parameter=register")
        )

    # Authorization
    def authorize(self):
        self.browser.get('%s%s' % (self.live_server_url, "/auth/login"))
        self.browser.find_element_by_id("id_username").send_keys("timurgrunge@gmail.com")
        self.browser.find_element_by_id("id_password").send_keys("123qwe")
        self.browser.find_element_by_tag_name("form").submit()
        time.sleep(2)
        self.browser.get(
            '%s%s' % (self.live_server_url, "/auth/user/profile")
        )
        time.sleep(2)






