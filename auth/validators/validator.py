from django.contrib.auth.models import User

from auth.models import Student
from courses.models import Subscribe


def user_already_logined(request):
    try:
        if request.user.is_authenticated:
            user = User(user=request.user)
            return False
        else:
            return True
    except:
        return False