import os
from datetime import datetime
from datetime import timezone

from PIL import Image, ImageDraw
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.forms import model_to_dict
from django.http import Http404, HttpResponseRedirect
from django.http import HttpResponse
from django.http import HttpResponseRedirect as Redirect
from django.shortcuts import render, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.template.context_processors import csrf
from django.urls import reverse
from django.contrib.auth.models import User, Group, Permission
from django.urls import reverse_lazy
from django.contrib import messages
from auth.forms import AuthForm, RegistrationFormUser, CompanyForm, UserForm, StudentForm
from django.db.utils import IntegrityError
from django.core.mail import send_mail
from auth.validators.decorator import user_passes_login, login_required_24hours
from auth.validators.validator import user_already_logined
from auth.models import Student
from courses.models import Company, Subscribe, TeachersGroup, Course, Certificates
from project.settings import SALT, PDF_ROOT
from django.db.models import Q


@user_passes_login(user_already_logined, login_url=reverse_lazy('courses:main'))
def logIn2(request):
    args = {}
    form = AuthForm()
    args['form'] = form
    args['next'] = None
    if request.GET.get('next'):
        args['next'] = request.GET.get('next')
    if request.POST:
        args.update(csrf(request))
        form = AuthForm(data=request.POST)
        args['form'] = form
        if form.is_valid():
            if datetime.now(
                    timezone.utc).timestamp() - form.get_user().date_joined.timestamp() < 86400 or form.get_user().email == 'True':
                login(request, form.get_user())
            else:
                messages.add_message(request, messages.ERROR, 'Подтвердите свою регистрацию')
                return render(request, 'login.html', args)
            if form.get_user().is_superuser:
                # Admins
                return Redirect('/admin/')
            else:
                user = User(id=form.get_user().id)
                # Please add the group in your DB for solve this problem (auth_group -> Moderators)
                # No Group matches the given query.
                group = get_object_or_404(Group, name="Moderators")
                if group in user.groups.all():
                    # Moderators
                    return Redirect(reverse('auth:profile'))
                else:
                    # Students
                    if args['next'] is not None:
                        return Redirect(args['next'])
                    else:
                        return Redirect(reverse('courses:main'))
        else:
            messages.add_message(request, messages.ERROR, 'Неправильный email или пароль.')
            return render(request, 'login.html', args)
    else:
        return render(request, 'login.html', args)


@user_passes_login(user_already_logined, login_url=reverse_lazy('courses:main'))
def register2(request):
    argc = {}
    argc['form'] = RegistrationFormUser()
    if request.POST:
        argc.update(csrf(request))
        user = User()
        user.email = 'False'
        form = RegistrationFormUser(request.POST, instance=user)
        argc['form'] = form
        if form.is_valid():
            user = None
            try:
                user = form.save(commit=True)
            except IntegrityError:
                messages.add_message(request, messages.ERROR, 'Этот email уже занят')
                return render(request, 'registration.html', argc)
            if user is not None:
                messages.add_message(request, messages.SUCCESS, 'Пожалуйста, подтвердите регистрацию, '
                                                                'перейдя по ссылке в письме')
                form = AuthForm()
                form.username = user.username
                return render(request, 'login.html', {'form': form})
            else:
                messages.add_message(request, messages.WARNING, 'Введите коректный email и пароль\'s')
                return render(request, 'registration.html', argc)
        else:
            messages.add_message(request, messages.ERROR, 'Ошибка')
            return render(request, 'registration.html', argc)
    else:
        return render(request, 'registration.html', argc)


@login_required_24hours(login_url=reverse_lazy('auth:logout'))
def profile(request):
    # if request.user.is_superuser:
    #   return HttpResponseRedirect(reverse('admin:index'))
    args = {}
    user = request.user
    student = get_object_or_404(Student, pk=user.student_user.id)
    try:
        args['company'] = Company.objects.get(user=student)
    except Exception:
        args['company'] = None
    args['student'] = student
    subscribe = Subscribe.objects.filter(student=student)
    if subscribe.exists():
        args['subscribe'] = subscribe
    else:
        pass
    # if Teacher
    teachers_group = TeachersGroup.objects.filter(teachers=request.user.student_user)
    args['teacher_group'] = teachers_group
    return render(request, 'profile.html', args)


@login_required_24hours(login_url=reverse_lazy('auth:logout'))
def proposal(request):
    subject = 'Proposal to moderator in EduSphere'
    message = '%s want to be moderator in educationsp.heroku.com .' % request.user.username
    users = User.objects.filter(is_superuser=True)
    if User.objects.filter(is_superuser=True).exists():
        send_mail(subject, message, request.user.username, [request.user.username, 'educationsphere5@gmail.com'],
                  fail_silently=False)
    return HttpResponse('Email to be moderator sended.')


def user_logout(request):
    logout(request)
    return Redirect(reverse('auth:login'))


@login_required_24hours(login_url=reverse_lazy('auth:logout'))
def proposal_to_be_company(request):
    if request.method == 'POST':
        f = CompanyForm(request.POST)
        if f.is_valid():
            try:
                student = Student.objects.get(user=request.user)
            except:
                raise Http404('User must be student!')
            if f.is_valid_errors(request):
                messages.add_message(request, messages.ERROR, f.is_valid_errors(request))
                return render(request, 'proposal_to_be_company.html', {'form': f})
            else:
                company = Company(user=student)
                f = CompanyForm(request.POST, instance=company)
                if (not Company.objects.filter(user=student).exists() and (
                        not Company.objects.filter(name=request.POST['name']).exists())):
                    h = hash(SALT + str(hash(student)))
                    if h < 0:
                        h = -h
                    company.hash_code = str(h)
                    url = 'auth/user/proposal/%s%s' % (company.hash_code, '?parameter=company')
                    f = CompanyForm(request.POST, instance=company)
                    # f.sendmail(student, url)
                    company.email_is_sended = True
                    subject = 'Proposal to be Company/Author in EduSphere'
                    message = '%s want to be company' % student.user.username
                    message += '\n That allow click to url https://educationsp.herokuapp.com/auth/user/proposal/%s' % (
                        str(h) + '?parameter=company')
                    send_mail(subject, message, student.user.username, ['educationsphere5@gmail.com'],
                              fail_silently=False)
                    f.save()
                    return HttpResponse(
                        "Request was sended, please, wait, when the administrator confirms the request.")
                else:
                    messages.add_message(request, messages.ERROR,
                                         "One company is already registered to this profile or this company name is occupied")
                    return render(request, 'proposal_to_be_company.html', {'form': f})
        else:
            messages.add_message(request, messages.ERROR, "Form is not valid, please enter data again!")
            return render(request, 'proposal_to_be_company.html', {'form': f})
    else:
        f = CompanyForm()
    return render(request, 'proposal_to_be_company.html', {'form': f})


@login_required_24hours(login_url=reverse_lazy('auth:logout'))
def confirmCompany(request, hash):
    if request.method == 'GET':
        if request.GET['parameter'] == 'company':
            path = request.get_full_path()
            try:
                company_for_user = Company.objects.get(hash_code=hash)
                student = None
                if request.user.is_superuser:
                    user_perms = company_for_user.user.user.user_permissions
                    permission = Permission.objects.filter(Q(codename='add_tasks')
                                                           | Q(codename='delete_tasks')
                                                           | Q(codename='change_tasks')
                                                           | Q(codename='add_course')
                                                           | Q(codename='delete_course')
                                                           | Q(codename='change_course')
                                                           | Q(codename='add_lection')
                                                           | Q(codename='delete_lection')
                                                           | Q(codename='change_lection')
                                                           | Q(codename='add_answers')
                                                           | Q(codename='delete_answers')
                                                           | Q(codename='change_answers')
                                                           | Q(codename='add_teachersgroup')
                                                           | Q(codename='change_teachersgroup')
                                                           | Q(codename='delete_teachersgroup')
                                                           | Q(codename='add_finaltaskinfinaltest')
                                                           | Q(codename='change_finaltaskinfinaltest')
                                                           | Q(codename='delete_finaltaskinfinaltest')
                                                           | Q(codename='add_studentfinaltestanswers')
                                                           | Q(codename='change_studentfinaltestanswers')
                                                           | Q(codename='delete_studentfinaltestanswers'))

                    for perm in permission:
                        user_perms.add(perm)
                    company_for_user.user.user.is_staff = True
                    company_for_user.email_is_sended = True
                    company_for_user.user.user.save()
                    company_for_user.is_confirm()
                    return Redirect('/admin')
                else:
                    student = request.user.student_user
                if company_for_user.user == student and company_for_user.email_is_sended is False:
                    company_for_user.email_is_sended = True
                    subject = 'Proposal to be Company/Author in EduSphere'
                    message = '%s want to be company' % student.user.username
                    message += '\n That allow click to url https://educationsp.herokuapp.com/auth/user/proposal/%s' % (
                        str(hash) + '?parameter=company')
                    send_mail(subject, message, student.user.username, ['educationsphere5@gmail.com'],
                              fail_silently=False)
                    company_for_user.save()
                    return Redirect(reverse('auth:profile'))
                else:
                    raise Http404('Wrong user!')
            except:
                raise Http404('Not found!')
        else:
            raise Http404('Oups!')
    else:
        raise Http404('What is that!')


def confirmUser(request, userhash):
    if request.user.is_authenticated:
        if request.GET['parameter'] == 'register':
            try:
                user = User(id=request.user.id)
                student = Student(user=user)
                if str(student.student_hash) == str(userhash):
                    user.email = 'True'
                    user.save()
                    return Redirect(reverse('auth:profile'))
                else:
                    raise Http404('Unknown hash code!')
            except:
                raise Http404('You must be student')
    else:
        try:
            users = User.objects.filter(email='False')
            for user in users:
                if str(user.student_user.student_hash) == str(userhash):
                    user.email = 'True'
                    user.save()
            return Redirect(reverse('auth:login'))
        except:
            return Redirect(reverse('auth:login'))


@login_required_24hours(login_url=reverse_lazy('auth:logout'))
def profileEdit(request):
    user = request.user
    # if user.is_superuser:
    #    return HttpResponseRedirect(reverse('admin:index'))
    student = Student.objects.get(user=user)
    args = {}
    args['birthday'] = student.birthday
    if request.method == "POST":
        usrform = UserForm(request.POST, instance=user)
        stform = StudentForm(request.POST, request.FILES, instance=student)
        if usrform.is_valid() and stform.is_valid():
            user = usrform.save()
            student = stform.saveForm(student.id)
            student.user = user
            student.save()
            return Redirect(reverse('auth:profile'))
        else:
            args['student'] = student
            args['usrform'] = usrform
            args['stform'] = stform
            return render(request, 'profile_edit.html', args)
    else:
        args['usrform'] = UserForm(instance=user)
        args['stform'] = StudentForm(instance=student)
        args['student'] = student
        return render(request, "profile_edit.html", args)


@login_required_24hours(login_url=reverse_lazy('auth:logout'))
def deleteCompany(request, hash):
    if request.method == 'POST':
        company_name = request.POST['company_name']
        company = get_object_or_404(Company, name=company_name)
        if request.user.student_user == company.user:
            perms = request.user.user_permissions.all()
            for i in perms:
                request.user.user_permissions.remove(i)
            request.user.save()
            company.delete()
        else:
            raise Http404("Ooops! Somethin was wrong!")
        return Redirect(reverse_lazy('auth:profile'))
    else:
        raise Http404()


def un_subscribe(request):
    if request.method == 'POST':
        teacher_group_name = request.POST['teacher_group_name']
        company = get_object_or_404(Company, name=teacher_group_name)
        if request.user.student_user in company.company_teachers.teachers.all():
            company.company_teachers.teachers.remove(request.user.student_user)
        else:
            raise Http404("Ooops! Somethin was wrong!")
        return Redirect(reverse_lazy('auth:profile'))
    else:
        raise Http404()


def generate_certificate(student_id, course_id):
    im = Image.open(PDF_ROOT + '\image.jpg')
    if im.mode != "RGB":
        im = im.convert("RGB")
    draw = ImageDraw.Draw(im)
    red = (64, 64, 127)
    text_pos = (580, 120)
    text = "Certificate"
    draw.text(text_pos, text, fill=red)
    course_name = Course.objects.get(id=course_id).name
    student_name = Student.objects.get(id=student_id).user.username
    student_points = Subscribe.objects.get(course_id=course_id,
                                           student=Student.objects.get(id=student_id)).final_test_points
    control_points = Course.objects.get(id=course_id).control_final_test_points
    result = int(round((student_points / control_points), 2) * 100)
    text_pos = (450, 170)
    text = "%s has completed course %s with score %s of 100" % (student_name, course_name, str(result))
    draw.text(text_pos, text, fill=red)
    del draw
    path = PDF_ROOT + '\%s' % (student_name) + '\%s.jpeg' % (course_name)
    path_for_db = 'pdf/%s/%s.jpeg' % (student_name, course_name)
    Certificates.objects.create(student_id=student_id, course_id=course_id, image=path_for_db)
    directory = PDF_ROOT + '\%s' % (student_name)
    if not os.path.exists(directory):
        os.makedirs(directory)
    Image.Image.save(im, path)


@login_required_24hours(login_url=reverse_lazy('auth:logout'))
def show_certificates(request):
    student_exists = Student.objects.filter(user=request.user).exists()
    if not student_exists:
        return HttpResponseRedirect(reverse('admin:index'))
    if 'course_id' in request.GET:
        course_name = Course.objects.get(id=request.GET['course_id']).name
        student_name = request.user.username
        path = PDF_ROOT + '\%s\%s.jpeg' % (student_name, course_name)
        f = open(path, 'rb')
        response = HttpResponse(f.read(), content_type='application/jpeg')
        response['Content-Disposition'] = 'attachment; filename="%s-%s.jpeg"' % (student_name, course_name)
        return response
    else:
        student = request.user.student_user
        certificates = []
        subscribes_of_user = Subscribe.objects.filter(student=student)
        for subscribe in subscribes_of_user:
            if subscribe.final_test_points >= subscribe.course.control_final_test_points:
                if not Certificates.objects.filter(student=student, course=subscribe.course).exists():
                    generate_certificate(student.id, subscribe.course.id)
                    certificates.append(Certificates.objects.get(student=student, course=subscribe.course))
                else:
                    certificates.append(Certificates.objects.get(student=student, course=subscribe.course))
        return render(request, 'courses/show_certificates.html', {'certificates': certificates})
