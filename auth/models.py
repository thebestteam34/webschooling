import os

from django.contrib.auth.models import User
from django.db import models


def user_folder(instance, filename):
    return 'users_img/' + str(instance.user.username) + '/' + str(filename)


class Student(models.Model):
    class Meta:
        db_table = 'students'
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'

    photo = models.ImageField(upload_to=user_folder, default='profile.png', blank=True)
    user = models.OneToOneField(User, related_name='student_user', on_delete=models.CASCADE,
                                verbose_name='Пользователь')
    city = models.CharField(max_length=40, null=True, blank=True, verbose_name='Город')
    university = models.CharField(max_length=40, null=True, blank=True, verbose_name='Место обучения')
    birthday = models.DateField(null=True, blank=True, verbose_name='Дата рождения')
    student_hash = models.TextField(null=False)

    def __str__(self):
        return self.user.first_name + self.user.last_name
