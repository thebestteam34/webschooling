from __future__ import absolute_import

import json

from django import template
from django.forms import Form
from django.utils.safestring import mark_safe

import cloudinary
from cloudinary import CloudinaryResource, utils, uploader
from cloudinary.forms import CloudinaryJsFileField, cl_init_js_callbacks
from cloudinary.compat import PY3

register = template.Library()


@register.filter
def split_tag(string):
    try:
        number_format = string.rindex('.')
        number = string.rindex('_')

        return string[0:number] + string[number_format:]
    except Exception:
        return string


@register.simple_tag(name='cloudinary', takes_context=True)
def cloudinary_tag(context, image, options_dict=None, **options):
    if options_dict is None:
        options = dict(**options)
    else:
        options = dict(options_dict, **options)
    try:
        if context['request'].is_secure() and 'secure' not in options:
            options['secure'] = True
    except KeyError:
        pass
    if not isinstance(image, CloudinaryResource):
        image = CloudinaryResource(image)
    return mark_safe(image.image(**options))
