var frm = $('#post-form');
var csrftoken = getCookie('csrftoken');
var comment = $('#textarea_message');

$('#submit_comment').click(function (e) {
    if ($('#hidden').val()=="aaa") {
        e.preventDefault();
        $.ajax({
            type: "POST",
            data: {
                comment_message: $('#textarea_message').val(),
                superuser: $('#hidden').val(),
                csrfmiddlewaretoken: getCookie('csrftoken')
            },
            success: function (data) {
                if (data['error']) {
                    alert (data['error'])
                }else {
                    $('#list_comments').prepend(
                        '<li><div class="comment-avatar"><img src="' + data['comment_user_photo'] +
                        '"alt=""></div> <div class="comment-box"> <div class="comment-head">' +
                        '<h6 class="comment-name"><a href="#">' + data['comment_username'] + '</a></h6>' +
                        '<span>' + data['comments_pub_date'] + '</span> <i class="fa fa-heart">' + data['comment_like'] + '</i>' +
                        '</div> <div class="comment-content">' + data['comment_messaage'] + ' </div> </div></li>'
                    );
                    $('#textarea_message').val("");
                }

            },
            error: function (data) {
                alert("Упс..")
            }

        });
        return false;
    }
});



var message = $('#new-review');
$('#submit').click(function (e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: frm.attr('action'),
        data: {
            comment: $('#new-review').val(),
            csrfmiddlewaretoken: getCookie('csrftoken')
        },
        success: function (data) {
             if (data['error']) {
                    alert (data['error'])
                }else {
                 $('#reviews').prepend(
                     ' <div class="media"> <p class="pull-right" style="margin: 5px">  <small>'
                     + data['date']
                     + '</small> </p> <a class="media-left" href="#">' +
                     '<img class="img" height="100"width="100" src="' + data['photo'] +
                     '"> </a> <div class="media-body"> <h4 class="media-heading user_name">'
                     + data['name'] +
                     '</h4>' + data['message'] +
                     '<p> <small><a href="">Мне нравится</a> - <a href="">Поделиться</a></small> </p> </div> </div>'
                 );
                 $('#count').html(data['count'] + ' comments');
                 $('#new-review').val("");
             }
        },
        error: function (data) {
            alert("Необходимо пройти 80% курса")
        }

    });
    return false;
});


function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


/*
 The functions below will create a header with csrftoken
 */

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}

$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }

});