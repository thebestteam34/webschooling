var frm = $('#proposal-form');
var csrftoken = getCookie('csrftoken');


$('#submit').click(function (e) {
    var name = $('#company_name').val();
    var about = $('#about').val();
    e.preventDefault();
    if (name == "" || name == null ||
        about == "" || about == null) {
        $('#errors').text("Заполните все поля!")
    } else {
        $.ajax({
            type: "POST",
            url: frm.attr('action'),
            data: {
                name: name,
                about: about,
                csrfmiddlewaretoken: getCookie('csrftoken')
            },
            success: function (data) {
                if (!data.trim().startsWith("<html")) {
                    $('#myModal').modal('show');
                } else {
                    $('#errors').text("Компания с таким названием уже существует")
                }
                console.log(data)
            },
            error: function (data) {
                alert("Ошибка!")
            }

        });
    }

    return false;
});


function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


/*
 The functions below will create a header with csrftoken
 */

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}

$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }

});