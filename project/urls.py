from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from auth import views
import auth.views
from courses import views
from project import settings

urlpatterns = [
    # Examples:
    # url(r'^$', 'project.views.home', name='home'),
    url(r'^admin/', admin.site.urls, name='admin'),
    url(r'^auth/', include('auth.urls', namespace='auth')),
    url(r'^', include('courses.urls', namespace='courses'))

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
