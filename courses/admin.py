import hashlib
from itertools import chain

import cloudinary
import cloudinary.uploader
import cloudinary.api
from django.contrib import admin, messages
from django.contrib.auth.models import Permission, User
from django.core.exceptions import ValidationError
from django.http import Http404
from django.shortcuts import get_object_or_404

from auth.models import Student
from .models import Course, Tasks, Answers, Lection, Review, FavoriteCourses, Subscribe, TeachersGroup, \
    FinalTaskInFinalTest, FavoriteReview, StudentFinalTestAnswers
from courses.forms import LectionAdminForm, AnswerForm, CourseFormAdmin, TeachersGroupForm, FinalTestForm
from django.db.models import ManyToManyField, Q, QuerySet
from django.db.models import TextField
from .models import Course, Tasks, Answers, Lection, Company, FavoriteCourses, Review
from ckeditor.widgets import CKEditorWidget


class LecInLine(admin.StackedInline):
    model = Lection
    form = LectionAdminForm
    show_change_link = ('name')
    min_num = 1
    ordering = ['order']
    formfield_overrides = {TextField: {'widget': CKEditorWidget}}


class TasksAdminStacked(admin.StackedInline):
    model = Tasks
    show_change_link = ('question')
    filter_horizontal = ('answers',)
    ordering = ['lection']
    formfield_overrides = {TextField: {'widget': CKEditorWidget}}

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if not request.user.is_superuser:
            # Company user
            if Company.objects.filter(user=request.user.student_user).exists():
                if db_field.name == "answers":
                    kwargs["queryset"] = Answers.objects.filter(
                        Q(all_answers__lection__course__creater__user__user=request.user) | Q(
                            creater=request.user.student_user) | Q(
                            creater__teacher_group__company=request.user.student_user.company_user)).distinct()
                    return super(TasksAdminStacked, self).formfield_for_manytomany(db_field, request, **kwargs)
            else:
                # Teachers
                if db_field.name == "answers":
                    kwargs['queryset'] = Answers.objects.none()
                    for group in request.user.student_user.teacher_group.all():
                        for teachers in group.teachers.all():
                            kwargs["queryset"] = kwargs["queryset"] | Answers.objects.filter(
                                Q(creater__user=teachers.user) | Q(
                                    creater__company_user=group.company)).distinct()
                    return super(TasksAdminStacked, self).formfield_for_manytomany(db_field, request, **kwargs)
        else:
            # Admin
            if db_field.name == "answers":
                kwargs["queryset"] = Answers.objects.all().distinct()
                return super(TasksAdminStacked, self).formfield_for_manytomany(db_field, request, **kwargs)


class LectionAdmin(admin.ModelAdmin):
    form = LectionAdminForm
    ordering = ['course', 'order']
    formfield_overrides = {TextField: {'widget': CKEditorWidget}}
    list_display = ('get_name', 'order', 'get_is_final', 'get_course', 'get_company')
    list_display_links = ('get_name', 'get_course', 'get_company')
    search_fields = ['name', 'course__name', 'course__creater__name']
    inlines = [TasksAdminStacked, ]

    # Links
    def get_is_final(self, obj):
        if obj.is_final:
            return True

    get_is_final.short_description = "Финальный тест"
    get_is_final.order_field = 'is_final'
    get_is_final.allow_tags = True

    def get_name(self, obj):
        return obj.__str__()

    get_name.short_description = "Название лекции"
    get_name.admin_order_field = "name"

    def get_course(self, obj):
        return '<a href="/admin/courses/course/%s/change/">%s</a>' % (
            obj.course.id, obj.course.__str__())

    get_course.short_description = "Course"
    get_course.admin_order_field = "course"
    get_course.allow_tags = True

    def get_company(self, obj):
        return '<a href="/admin/courses/company/%s/change/">%s</a>' % (
            obj.course.creater.id, obj.course.creater.__str__())

    get_company.short_description = "Company"
    get_company.admin_order_field = "course__creater"
    get_company.allow_tags = True

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "course":
            if not request.user.is_superuser:
                if Company.objects.filter(user=request.user.student_user).exists():
                    # Creater course
                    kwargs["queryset"] = Course.objects.filter(creater__user__user=request.user)
                else:
                    # Teachers
                    kwargs["queryset"] = Course.objects.none()
                    for group in request.user.student_user.teacher_group.all():
                        kwargs["queryset"] = kwargs["queryset"] | Course.objects.filter(
                            creater__user__user=group.company.user.user)
            else:
                # Admin
                kwargs["queryset"] = Course.objects.all()
        return super(LectionAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        qs = super(LectionAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        else:
            if Company.objects.filter(user=request.user.student_user).exists():
                # Creater lection
                return qs.filter(course__creater__user__user=request.user)
            else:
                # Teacher
                lections = Lection.objects.none()
                if request.user.student_user.teacher_group.exists():
                    for group in request.user.student_user.teacher_group.all():
                        lections = lections | Lection.objects.filter(
                            course__creater=group.company)
                else:
                    return Http404("Service exception!")
            return lections


admin.site.register(Lection, LectionAdmin)


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_user', 'is_confirmed',)
    list_display_links = ('name', 'get_user',)
    ordering = ['name', ]

    def get_user(self, obj):
        return "<a href=\"/admin/auth2/student/%s/change/\">%s</a>" % (obj.user.id, obj.user.user.first_name)

    get_user.short_description = "Владелец"
    get_user.admin_order_field = "user"
    get_user.allow_tags = True

    permission = Permission.objects.filter(Q(codename='add_tasks')
                                           | Q(codename='delete_tasks')
                                           | Q(codename='change_tasks')
                                           | Q(codename='add_course')
                                           | Q(codename='delete_course')
                                           | Q(codename='change_course')
                                           | Q(codename='add_lection')
                                           | Q(codename='delete_lection')
                                           | Q(codename='change_lection')
                                           | Q(codename='add_answers')
                                           | Q(codename='delete_answers')
                                           | Q(codename='change_answers')
                                           | Q(codename='add_teachersgroup')
                                           | Q(codename='change_teachersgroup')
                                           | Q(codename='delete_teachersgroup')
                                           | Q(codename='add_finaltaskinfinaltest')
                                           | Q(codename='change_finaltaskinfinaltest')
                                           | Q(codename='delete_finaltaskinfinaltest')
                                           | Q(codename='add_studentfinaltestanswers')
                                           | Q(codename='change_studentfinaltestanswers')
                                           | Q(codename='delete_studentfinaltestanswers'))

    def delete_model(self, request, obj):
        old_company = get_object_or_404(Company, pk=obj.id)
        old_user_perms = old_company.user.user.user_permissions
        for p in self.permission:
            old_user_perms.remove(p)
        return super(CompanyAdmin, self).delete_model(request, obj)

    def save_model(self, request, obj, form, change):
        user_perms = obj.user.user.user_permissions
        old_company = None
        if change:
            old_company = get_object_or_404(Company, pk=obj.id)
            old_user_perms = old_company.user.user.user_permissions
        for p in self.permission:
            user_perms.add(p)
            if change and 'user' in form.changed_data:
                old_user_perms.remove(p)

        obj.user.user.is_staff = True
        obj.user.user.email_is_sended = True
        obj.user.user.save()
        obj.is_confirm()
        return super(CompanyAdmin, self).save_model(request, obj, form, change)


class CourseModel(admin.ModelAdmin):
    ordering = ['creater', '-date_begining']
    search_fields = ('name', 'creater__name',)
    list_display = ('name', 'get_company', 'direction', 'date_begining', 'date_ending', 'image')
    list_display_links = ('name', 'get_company',)
    formfield_overrides = {TextField: {'widget': CKEditorWidget}}
    form = CourseFormAdmin

    def get_company(self, obj):
        return '<a href="/admin/courses/company/%s/change/">%s</a>' % (
            obj.creater.id, obj.creater.__str__())

    get_company.short_description = "Компания"
    get_company.admin_order_field = "creater"
    get_company.allow_tags = True

    def save_model(self, request, obj, form, change):
        if 'image' in form.changed_data:
            try:
                # CloudInary images
                s = str(obj.image).split('.')
                e = str()
                for i in range(0, len(s) - 1):
                    e += s[i] + '.'
                filename = e[0:-1]
                slash = filename.rfind("/")
                m = hashlib.md5()
                m.update(filename[slash + 1:].encode("UTF-8"))
                md5name = m.hexdigest()
                obj.image.name = filename[0:slash + 1] + md5name + obj.image.name[obj.image.name.rfind("."):]
                obj.save()
                cloudinary.uploader.upload(obj.image, public_id='media/' + obj.image.name[0:obj.image.name.rfind(".")])
            except:
                pass

        if request.user.is_superuser:
            form.save(form)
            if not Subscribe.objects.filter(course=obj, student=request.user.student_user).exists():
                sb = Subscribe(course=obj, student=request.user.student_user,
                               points=9999, lection_status=None, final_test_points=9999)
                sb.save()
        else:
            obj.creater = request.user.student_user.company_user
            obj.save()
            # If not superuser - auto subscribe
            if not Subscribe.objects.filter(course=obj, student=request.user.student_user).exists():
                sb = Subscribe(course=obj, student=request.user.student_user,
                               points=9999, lection_status=None, final_test_points=9999)
                sb.save()
            superusers = User.objects.filter(is_superuser=True)
            for superuser in superusers:
                if not Subscribe.objects.filter(course=obj, student=superuser.student_user).exists():
                    sb = Subscribe(course=obj, student=superuser.student_user,
                                   points=9999, lection_status=None, final_test_points=9999)
                    sb.save()

        return obj

    def get_form(self, request, obj=None, **kwargs):
        self.exclude = []
        # self.instance.pk should be None if the object hasn't yet been persisted
        if not request.user.is_superuser:
            self.exclude.append('creater')
        # For courses
        # try:
        #    if obj.course_final_task
        # except:
        #    pass
        return super(CourseModel, self).get_form(request, obj, **kwargs)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "creater":
            if not request.user.is_superuser:
                kwargs["queryset"] = Company.objects.get(user__user=request.user)
            else:
                kwargs["queryset"] = Company.objects.all()

        if db_field.name == "next_course":
            previous_course = None
            prev_id = -1
            try:
                object_id = int(request.path.split('/')[4])
                try:
                    previous_course = Course.objects.filter(id=object_id)[0].course_next
                    if previous_course:
                        prev_id = previous_course.id
                except:
                    prev_id = -1
            except:
                object_id = None
            if not request.user.is_superuser:
                if Company.objects.filter(user=request.user.student_user).exists():
                    # Creater course
                    if object_id:
                        kwargs["queryset"] = Course.objects.filter(creater__user__user=request.user).exclude(
                            id=object_id).exclude(id=prev_id)
                    else:
                        kwargs["queryset"] = Course.objects.filter(creater__user__user=request.user)
                else:
                    # Teachers
                    if not object_id:
                        kwargs["queryset"] = Course.objects.filter(
                            creater__user__user=request.user.student_user.teacher_group.all()[0].company.user.user)
                    else:
                        kwargs["queryset"] = Course.objects.filter(
                            creater__user__user=request.user.student_user.teacher_group.all()[
                                0].company.user.user).exclude(id=object_id).exclude(id=prev_id)
            else:
                # Admin
                if not object_id:
                    kwargs["queryset"] = Course.objects.all()
                else:
                    kwargs["queryset"] = Course.objects.all().exclude(id=object_id).exclude(id=prev_id)
        return super(CourseModel, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        qs = super(CourseModel, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(creater__user__user=request.user)


admin.site.register(Course, CourseModel)


@admin.register(Tasks)
class TasksAdmin(admin.ModelAdmin):
    search_fields = ('question', 'lection__name')
    filter_horizontal = ('answers',)
    list_display_links = ('question', 'show_lection', 'get_course',)
    list_display = ('question', 'points', 'show_lection', 'get_course', 'get_company')
    ordering = ['lection__course__creater', 'lection__course', 'lection']

    # Cell's
    def get_course(self, obj):
        return '<a href="/admin/courses/course/%s/change/">%s</a>' % (
            obj.lection.course.id, obj.lection.course.__str__())

    get_course.short_description = "Course"
    get_course.admin_order_field = "lection__course"
    get_course.allow_tags = True

    def get_company(self, obj):
        return '<a href="/admin/courses/company/%s/change/">%s</a>' % (
            obj.lection.course.creater.id, obj.lection.course.creater.__str__())

    get_company.short_description = "Company"
    get_company.admin_order_field = "lection__course__creater"
    get_company.allow_tags = True

    # Links
    def show_lection(self, obj):
        return '<a href="/admin/courses/lection/%s/change/">%s</a>' % (obj.lection.id, obj.lection.__str__())

    show_lection.short_description = "Lection"
    show_lection.admin_order_field = "lection"
    show_lection.allow_tags = True

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "lection":
            if not request.user.is_superuser:
                if Company.objects.filter(user=request.user.student_user).exists():
                    # Creater course
                    kwargs["queryset"] = Lection.objects.filter(course__creater__user__user=request.user)
                else:
                    # Teachers
                    kwargs["queryset"] = Lection.objects.none()
                    for group in request.user.student_user.teacher_group.all():
                        kwargs["queryset"] = kwargs["queryset"] | Lection.objects.filter(
                            course__creater__user__user=group.company.user.user)
            else:
                # Admin
                kwargs["queryset"] = Lection.objects.all()
            return super(TasksAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if not request.user.is_superuser:
            # Company user
            if Company.objects.filter(user=request.user.student_user).exists():
                if db_field.name == "answers":
                    kwargs["queryset"] = Answers.objects.filter(
                        Q(all_answers__lection__course__creater__user__user=request.user) | Q(
                            creater=request.user.student_user) | Q(
                            creater__teacher_group__company=request.user.student_user.company_user)).distinct()
                    return super(TasksAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)
            else:
                # Teachers
                if db_field.name == "answers":
                    kwargs['queryset'] = Answers.objects.none()
                    for group in request.user.student_user.teacher_group.all():
                        for teachers in group.teachers.all():
                            kwargs["queryset"] = kwargs["queryset"] | Answers.objects.filter(
                                Q(creater__user=teachers.user) | Q(
                                    creater__company_user=group.company)).distinct()
                    return super(TasksAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)
        else:
            # Admin
            if db_field.name == "answers":
                kwargs["queryset"] = Answers.objects.all().distinct()
                return super(TasksAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)

    def get_queryset(self, request):
        qs = super(TasksAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            # Admin
            return qs
        else:
            # Company
            if Company.objects.filter(user=request.user.student_user).exists():
                return qs.filter(lection__course__creater__user__user=request.user)
            else:
                # Teachers

                tasks = Tasks.objects.none()
                if request.user.student_user.teacher_group.exists():
                    for group in request.user.student_user.teacher_group.all():
                        tasks = tasks | Tasks.objects.filter(
                            Q(lection__course__creater=group.company))
                else:
                    return Http404("Service exception!")
            return tasks


@admin.register(FinalTaskInFinalTest)
class FinalTaskInFinalTestAdmin(admin.ModelAdmin):
    search_fields = ('question', 'lection__name', 'lection__course__name', 'lection__course__creater__name')
    list_display_links = ('question', 'show_lection', 'get_course',)
    list_display = ('question', 'points', 'show_lection', 'get_course', 'get_company')
    ordering = ['lection__course__creater', 'lection__course', 'lection']

    # Cell's
    def get_course(self, obj):
        return '<a href="/admin/courses/course/%s/change/">%s</a>' % (
            obj.lection.course.id, obj.lection.course.__str__())

    get_course.short_description = "Course"
    get_course.admin_order_field = "lection__course"
    get_course.allow_tags = True

    def get_company(self, obj):
        return '<a href="/admin/courses/company/%s/change/">%s</a>' % (
            obj.lection.course.creater.id, obj.lection.course.creater.__str__())

    get_company.short_description = "Company"
    get_company.admin_order_field = "lection__course__creater"
    get_company.allow_tags = True

    # Links
    def show_lection(self, obj):
        return '<a href="/admin/courses/lection/%s/change/">%s</a>' % (obj.lection.id, obj.lection.__str__())

    show_lection.short_description = "Lection"
    show_lection.admin_order_field = "lection"
    show_lection.allow_tags = True

    # QuerySet
    def get_queryset(self, request):
        qs = super(FinalTaskInFinalTestAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        else:
            if Company.objects.filter(user=request.user.student_user).exists():
                return qs.filter(lection__course__creater__user__user=request.user)
            else:
                tasks = FinalTaskInFinalTest.objects.none()
                if request.user.student_user.teacher_group.exists():
                    for group in request.user.student_user.teacher_group.all():
                        tasks = tasks | FinalTaskInFinalTest.objects.filter(
                            Q(lection__course__creater=group.company))
                else:
                    return Http404("Service exception!")
            return tasks

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "lection":
            if not request.user.is_superuser:
                if Company.objects.filter(user=request.user.student_user).exists():
                    # Creater course
                    kwargs["queryset"] = Lection.objects.filter(course__creater__user__user=request.user, is_final=True)
                else:
                    # Teachers
                    kwargs["queryset"] = Lection.objects.none()
                    for group in request.user.student_user.teacher_group.all():
                        kwargs["queryset"] = kwargs["queryset"] | Lection.objects.filter(
                            course__creater__user__user=group.company.user.user,
                            is_final=True)
            else:
                # Admin
                kwargs["queryset"] = Lection.objects.filter(is_final=True)
            return super(FinalTaskInFinalTestAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def save_model(self, request, obj, form, change):
        if 'photo' in form.changed_data:
            try:
                m = hashlib.md5()
                m.update(obj.photo.name[0:obj.photo.name.rfind(".")].encode("UTF-8"))
                obj.photo.name = m.hexdigest() + obj.photo.name[obj.photo.name.rfind("."):]
                obj.save()
                # CloudInary images
                filename = str()
                path = obj.photo.name[0:obj.photo.name.rfind("/") + 1]
                cloudinary.uploader.upload(obj.photo,
                                           public_id='media/' + path + str(m.hexdigest()))
            except:
                pass
        return obj.save()


@admin.register(Answers)
class All_answersAdmin(admin.ModelAdmin):
    search_fields = ('answer_text', 'creater__user__first_name', 'all_answers__lection__name')
    list_display = ('answer_text', 'get_task_name', 'creater')
    list_display_links = ('answer_text', 'get_task_name')
    form = AnswerForm

    def save_model(self, request, obj, form, change):
        if not change:
            obj.creater = request.user.student_user
        return super(All_answersAdmin, self).save_model(request, obj, form, change)

    def get_task_name(self, obj):
        # TODO add exclude for independed for this lection task's
        all_tasks = Tasks.objects.filter(answers=obj)
        tag = str()
        for i in all_tasks:
            tag += '<a href="/admin/courses/tasks/%s/change/">%s</a> ' % (
                i.id, "<b>Task</b>: " + str(i.question) + " from <b>" + str(i.lection.name) + "</b> lection") + "<hr>"
        return tag[:len(tag) - 4]

    get_task_name.short_description = "Задача"
    get_task_name.admin_order_field = 'task_name'
    get_task_name.allow_tags = True

    def get_queryset(self, request):
        qs = super(All_answersAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs.order_by('all_answers__lection').distinct()
        else:
            if Company.objects.filter(user=request.user.student_user).exists():
                return qs.filter(Q(creater__user=request.user) | Q(
                    creater__teacher_group__company=request.user.student_user.company_user)).distinct()
            else:
                # Teacher
                all_answers = Answers.objects.none()
                if request.user.student_user.teacher_group.exists():
                    for group in request.user.student_user.teacher_group.all():
                        for teachers in group.teachers.all():
                            all_answers = all_answers | Answers.objects.filter(Q(creater__user=teachers.user) | Q(
                                creater__company_user=group.company)).distinct()
            return all_answers


@admin.register(FavoriteCourses)
class FavoriteCoursesAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'course':
            favorite_objects = FavoriteCourses.objects.all().values("course__id")
            kwargs["queryset"] = Course.objects.all().exclude(id__in=favorite_objects)
        return super(FavoriteCoursesAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


# Ответы студентов
@admin.register(StudentFinalTestAnswers)
class StudentFinalAnswersAdmin(admin.ModelAdmin):
    list_display_links = ('student', 'qtask',)
    list_display = ('student', 'qtask', 'is_solved')
    form = FinalTestForm
    fields = ('student', 'question', 'image_question', 'answer', 'photo', 'points', 'mark',)
    readonly_fields = ('student', 'answer',)

    def qtask(self, obj):
        return '<a href="/admin/courses/finaltaskinfinaltest/%s/change/">%s</a>' % (obj.task.id, obj.task.question)

    qtask.short_description = "Задача"
    qtask.admin_ordered_field = "task"
    qtask.allow_tags = True

    # Disable for add object
    def has_add_permission(self, request):
        return False

    def get_task(self, obj):
        return FinalTaskInFinalTest.objects.get(id=obj.task.id)

    def get_image_question(self, obj):
        return FinalTaskInFinalTest.objects.get(id=obj.task.id).photo

    get_image_question.allow_tags = True

    def get_form(self, request, obj=None, **kwargs):
        self.fields = ('student', 'question', 'image_question', 'answer', 'photo', 'points', 'mark',)
        self.exclude = []
        # Image Question Check
        c = self.get_image_question(obj)
        if c != None:
            c.name = "http://res.cloudinary.com/educationsphere/image/upload/v1494417475/media/%s" % c.name
            self.form.base_fields['image_question'].initial = c.name
        else:
            self.exclude.append('image_question')
            self.fields = tuple(f for f in self.fields if f != 'image_question')
        # Question
        self.form.base_fields['question'].initial = self.get_task(obj).question
        # Points
        self.form.base_fields['points'].initial = self.get_task(obj).points
        # Image or Text - student
        if obj.photo is not None:
            self.exclude.append('answer')
            self.fields = tuple(f for f in self.fields if f != 'answer')
        else:
            self.exclude.append('photo')
            self.fields = tuple(f for f in self.fields if f != 'photo')
        return super(StudentFinalAnswersAdmin, self).get_form(request, obj, **kwargs)

    def save_model(self, request, obj, form, change):
        if form.is_valid():
            if Subscribe.objects.filter(student=obj.student, course=obj.task.lection.course).exists():
                sub = Subscribe.objects.get(student=obj.student, course=obj.task.lection.course)
                sub.final_test_points += form.cleaned_data['mark']
                sub.save()
            obj.is_solved = True
            return super(StudentFinalAnswersAdmin, self).save_model(request, obj, form, change)

    # QuerySet
    def get_queryset(self, request):
        qs = super(StudentFinalAnswersAdmin, self).get_queryset(request)
        # Admin
        if request.user.is_superuser:
            return qs.filter(is_solved=False)
        # Company
        if Company.objects.filter(user=request.user.student_user).exists():
            return qs.filter(task__lection__course__creater__user=request.user.student_user, is_solved=False)
        else:
            # Teachers
            if request.user.student_user.teacher_group.exists():
                return qs.filter(
                    task__lection__course__creater=request.user.student_user.teacher_group.company, is_solved=False)


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    # TODO add search field
    # TODO realise
    pass


@admin.register(FavoriteReview)
class FavoriteReviewAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'review':
            favorite_objects = FavoriteReview.objects.all().values("review__id")
            kwargs["queryset"] = Review.objects.all().exclude(id__in=favorite_objects)
        return super(FavoriteReviewAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(TeachersGroup)
class TeachersGroupAdmin(admin.ModelAdmin):
    search_fields = ('name', 'company__name',)
    list_display = ('name', 'get_company',)
    list_display_links = ('name', 'get_company',)
    ordering = ['name', ]
    filter_horizontal = ('teachers',)
    form = TeachersGroupForm

    def get_company(self, obj):
        return '<a href="/admin/courses/company/%s/change/">%s</a>' % (obj.company.id, obj.company.name)

    get_company.short_description = 'Компания'
    get_company.admin_order_field = "company"
    get_company.allow_tags = True

    def get_form(self, request, obj=None, **kwargs):
        self.exclude = []
        # self.instance.pk should be None if the object hasn't yet been persisted
        if not request.user.is_superuser:
            self.exclude.append('company')
        return super(TeachersGroupAdmin, self).get_form(request, obj, **kwargs)

    def get_queryset(self, request):
        qs = super(TeachersGroupAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs.order_by('company').distinct()
        else:
            return qs.filter(company__user__user=request.user).distinct()

    def save_model(self, request, obj, form, change):
        try:
            users = form.cleaned_data['teachers'].all()
            if request.user.is_superuser:
                obj.company = form.cleaned_data['company']
            else:
                obj.company = request.user.student_user.company_user
            obj.save()
            permission = Permission.objects.filter(Q(codename='add_tasks')
                                                   | Q(codename='delete_tasks')
                                                   | Q(codename='change_tasks')
                                                   | Q(codename='add_lection')
                                                   | Q(codename='delete_lection')
                                                   | Q(codename='change_lection')
                                                   | Q(codename='add_answers')
                                                   | Q(codename='change_answers')
                                                   | Q(codename='delete_answers')
                                                   | Q(codename='add_finaltaskinfinaltest')
                                                   | Q(codename='change_finaltaskinfinaltest')
                                                   | Q(codename='delete_finaltaskinfinaltest')
                                                   | Q(codename='add_studentfinaltestanswers')
                                                   | Q(codename='change_studentfinaltestanswers')
                                                   | Q(codename='delete_studentfinaltestanswers'))

            for user in users:
                for perm in permission:
                    user.user.user_permissions.add(perm)
                    user.user.is_staff = True
                    user.email_is_sended = True
                    user.user.save()
                obj.teachers.add(user)
                for course in obj.company.course_set.all():
                    if not Subscribe.objects.filter(course=course, student=user).exists():
                        sb = Subscribe(course=course, student=user,
                                       points=9999, lection_status=None, final_test_points=9999)
                        sb.save()
            return obj
        except:
            raise Http404("Teachers group already exists")


@admin.register(Subscribe)
class SubscribeAdmin(admin.ModelAdmin):
    list_display = ('get_object2', 'get_student', 'course', 'points', 'lection_status', 'final_test_points')
    search_fields = ('student__user__username', 'course__name')
    list_display_links = ('get_object2', 'get_student',)

    def get_object2(self, obj):
        return '<a href="/admin/courses/subscribe/%s/change/">%s</a>' % (obj.id, obj)

    get_object2.allow_tags = True
    get_object2.short_description = "Подписка"
    get_object2.admin_order_field = "id"

    def get_student(self, obj):
        return '<a href="/admin/auth2/student/%s/change/">%s</a>' % (obj.student.id, obj.student.user.username)

    get_student.allow_tags = True
    get_student.short_description = "Студент"
    get_student.admin_order_field = "student"
