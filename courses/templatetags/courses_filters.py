from django.template.defaultfilters import stringfilter
from django.template.defaulttags import register
from requests import request

from courses.models import Tasks, StudentAnswers, FinalTaskInFinalTest, directions_of_training


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@register.filter
def is_checkbox(list, key):
    return Tasks.objects.get(id=key).type == 'checkbox'


@register.filter
def is_radio(list, key):
    return Tasks.objects.get(id=key).type == 'radio'


@register.filter
def is_text(list, key):
    return Tasks.objects.get(id=key).type == 'text'


@register.filter
def get_question(key):
    return Tasks.objects.get(id=key).question


@register.filter
def get_final_question(key):
    key = str(key)
    return FinalTaskInFinalTest.objects.get(id=int(key[5:])).question


@register.filter
def is_final(list, key):
    return str(key).find('photo') != -1

@register.filter
def has_photo(list,key):
    key = str(key)
    return FinalTaskInFinalTest.objects.get(id=int(key[5:])).photo

@register.filter
def answer_is_photo(list,key):
    key = str(key)
    return FinalTaskInFinalTest.objects.get(id=int(key[5:])).answer_is_photo

@register.filter
def get_photo(key):
    return FinalTaskInFinalTest.objects.get(id=int(str(key)[5:])).photo.url


@register.filter
@stringfilter
def is_right(string, key):
    return Tasks.objects.get(id=key).answers.filter(answer_text=string).filter(is_right=True).exists()


@register.filter
@stringfilter
def tostring(value):
    return str(value)


@register.simple_tag()
def correctDate(birthday):
    return birthday


@register.simple_tag()
def progress(sub):
    now = sub.points
    max = sub.course.control_points
    return '%s / %s' % (now, max)

@register.simple_tag()
def direction_name(direction_value):
    for d in directions_of_training:
        if d[0] == direction_value:
            return d[1]
    return None
