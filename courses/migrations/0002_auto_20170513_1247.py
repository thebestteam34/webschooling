# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-05-13 09:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lection',
            name='info',
            field=models.TextField(blank=True, max_length=10000, verbose_name='Лекционная информация'),
        ),
    ]
