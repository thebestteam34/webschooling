from django.conf.urls import include, url
from courses import views

# Ставьте норм урлы, чо как в деревне

urlpatterns = [
    url(r'^courses/all$', views.list_courses, name='all'),
    url(r'^courses/search', views.course_search, name="search"),
    url(r'^courses/(?P<course_id>\d+)$', views.show_course, name='one'),
    url(r'^courses/(?P<course_id>\d+)/lections$', views.list_lections, name='lections'),
    url(r'^courses/(?P<course_id>\d+)/lections/(?P<lection_id>\d+)$', views.show_lection, name='lection'),
    url(r'^courses/(?P<course_id>\d+)/lections/(?P<lection_id>\d+)/tasks$', views.show_tasks, name='lection_task'),
    url(r'^courses/(?P<course_id>\d+)/reviews$', views.reviews, name='reviews'),
    url(r'^courses/(?P<course_id>\d+)/test$', views.show_test, name='test'),
    url(r'^courses/(?P<course_id>\d+)/support', views.support, name='support'),
    #url(r'^courses/(?P<course_id>\d+)/')
    url(r'^$', views.main, name='main')
]
