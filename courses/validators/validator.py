from django.contrib.auth.decorators import user_passes_test

from auth.models import Student
from courses.models import Subscribe


def user_must_subscribe(request):
    try:
        if Student.objects.filter(user=request.user).exists():
            student = Student.objects.filter(user=request.user)
            path = request.path.split('/')
            course_id = int(path[2])
            if Subscribe.objects.filter(course=course_id,student=student).exists():
                return True
            else:
                return False
    except:
        return False