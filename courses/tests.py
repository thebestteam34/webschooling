import time

from django.contrib.auth.models import User
from django.test import TestCase

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver

from auth.models import Student
from courses.models import Tasks, Answers, Lection, Course, StudentAnswers


class CourseTests(StaticLiveServerTestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def test_search_h1_in_courses_template(self):
        self.browser.get('%s%s' % (self.live_server_url, '/courses/all'))
        h1 = self.browser.find_element_by_tag_name('h1')
        time.sleep(3)
        assert h1.text == 'Найти курс'

    def tearDown(self):
        self.browser.quit()


class StudentAnswerTestCase(TestCase):
    def setUp(self):
        User.objects.create_superuser(username="marsel", email="marsel@bb.ru", password="marsel887")
        Answers.objects.create(answer_text="One")
        Answers.objects.create(answer_text="Two")
        Answers.objects.create(answer_text="Three")

    def test_are_correct(self):
        user = User.objects.get(username="marsel")
        student = Student.objects.create(user=user)
        answer1 = Answers.objects.get(answer_text="One")
        answer2 = Answers.objects.get(answer_text="Two")
        answer3 = Answers.objects.get(answer_text="Three")
        course = Course.objects.create(creater=user, name="Math", direction="mm")
        lection = Lection.objects.create(course=course, name="Math", info="math")
        task = Tasks.objects.create(question="Question", points=3, lection=lection)
        task.right_answers.add(answer1, answer2)
        task.all_answers.add(answer1, answer2, answer3)
        student_answers = StudentAnswers.objects.create(student=student, task=task)
        student_answers.given_answers.add(answer1, answer2)
        self.assertTrue(student_answers.are_correct())
