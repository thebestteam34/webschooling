import datetime
import os

from django.contrib.auth.models import User
from django.db import models

from auth.models import Student

directions_of_training = (
    ('ALL', 'Все направления'),
    ('MNS', 'Математические и естественные науки'),
    ('ET', 'Инженерия, технологии и технические науки'),
    ('HMS', 'Здоровье и медицинские науки'),
    ('AS', 'Сельское хозяйство и сельскохозяйственные науки'),
    ('SS', 'Наука об обществе'),
    ('TS', 'Образование и наука о преподавании'),
    ('HS', 'Гуманитарные науки'),
    ('AC', 'Искусство и культура'),
)


# Create your models here.

def course_forlder(instance, filename):
    return 'courses_img/' + str(instance.name) + '/' + str(filename)


def task_folder(instance, filename):
    return 'tasks_img' + '/' + str(filename)


def user_task_folder(instance, filename):
    return 'users_img/' + str(instance.student.user.username) + "/" + str(filename)


def certificate_folder(instance, filename):
    return 'pdf/' + str(instance.student.user.username) + '/' + str(filename)


class Course(models.Model):
    class Meta:
        db_table = 'course'
        verbose_name = 'Курс'
        verbose_name_plural = 'Курсы'

    # Creater must been Company model now
    creater = models.ForeignKey('Company', on_delete=models.CASCADE, verbose_name='Компания курса')
    name = models.CharField(max_length=100, unique=True, null=False, blank=False, verbose_name='Название курса')
    about = models.TextField(max_length=3000, blank=True, null=True, verbose_name='О курсе')
    direction = models.CharField(max_length=3, choices=directions_of_training, verbose_name='Направление курса')
    date_begining = models.DateTimeField(default=datetime.datetime.today, null=True, verbose_name='Дата начала')
    date_ending = models.DateTimeField(null=True, verbose_name='Дата окончания')
    control_points = models.IntegerField(default=0, null=True,
                                         verbose_name='Минимальные очки за весь курс к допуску к теста')
    control_final_test_points = models.IntegerField(default=0, null=True,
                                                    verbose_name='Минимальные очки за итоговый тест')
    image = models.ImageField(upload_to=course_forlder, default='default_course.png', blank=True)
    next_course = models.OneToOneField('Course', null=True, blank=True, verbose_name='Следующий курс',
                                       related_name='course_next')

    def __str__(self):
        return '%s' % self.name


class Subscribe(models.Model):
    course = models.ForeignKey('Course', on_delete=models.CASCADE, related_name='sub_course', verbose_name='Курс')
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='sub_student', verbose_name='Студент')
    points = models.IntegerField(blank=False, null=False, default=0, verbose_name='Очки студента')
    lection_status = models.ForeignKey('Lection', null=True, verbose_name='Текущая лекция')
    final_test_points = models.IntegerField(blank=False, null=False, default=-1,
                                            verbose_name='Очки студента за итоговый тест')

    class Meta:
        db_table = 'subscribes'
        unique_together = (('course', 'student'),)
        verbose_name = 'Подписка'
        verbose_name_plural = 'Подписки'

    def __str__(self):
        return self.student.user.first_name


# Component of course
class Lection(models.Model):
    course = models.ForeignKey('Course', on_delete=models.CASCADE, null=True, blank=False, related_name='lec_course',
                               default=None, verbose_name='Курс')
    name = models.CharField(max_length=100, blank=True, null=False, verbose_name='Название лекции')
    info = models.TextField(max_length=10000, blank=True, null=False, verbose_name='Лекционная информация')
    order = models.IntegerField(default=1, verbose_name='Порядковый номер')
    youtube_url = models.TextField(max_length=500, null=True, blank=True, verbose_name='YouTube-ссылка')
    is_final = models.BooleanField(default=False, verbose_name='Выберите v , если это лекция для финального теста.')

    class Meta:
        verbose_name = 'Лекцию'
        verbose_name_plural = 'Лекции'
        unique_together = (('course', 'order'),)

    def __str__(self):
        return '%s %s' % (self.name, '[Final Test]' if self.is_final else '')


# Who create course
class Company(models.Model):
    class Meta:
        verbose_name = 'Компания'
        verbose_name_plural = 'Компании'

    user = models.OneToOneField(Student, related_name='company_user', on_delete=models.CASCADE,
                                verbose_name='Пользователь')
    name = models.CharField(max_length=200, unique=True, null=False, blank=True, verbose_name='Название компании')
    about = models.TextField(max_length=45000, null=True, verbose_name='О компании')
    is_confirmed = models.BooleanField(default=False, verbose_name='Подтверждена')
    hash_code = models.TextField(default='')
    email_is_sended = models.BooleanField(default=False)

    def __str__(self):
        return '%s' % self.name

    def is_confirm(self):
        self.is_confirmed = True
        self.save()


class TeachersGroup(models.Model):
    class Meta:
        verbose_name = 'Группу'
        verbose_name_plural = 'Группа учителей'

    name = models.TextField(default='Учителя')
    company = models.OneToOneField('Company', related_name='company_teachers', on_delete=models.CASCADE,
                                   verbose_name='Компания', unique=True)
    teachers = models.ManyToManyField(Student, related_name='teacher_group', null=True)

    def __str__(self):
        return self.name


class Tasks(models.Model):
    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'

    TYPES = (
        ('checkbox', 'checkbox'),
        ('radio', 'radio'),
        ('text', 'text')
    )

    question = models.TextField(verbose_name='Вопрос')
    answers = models.ManyToManyField('Answers', related_name="all_answers", verbose_name='Все ответы')
    points = models.FloatField(verbose_name='Максимальное кол-во очков за задание')
    lection = models.ForeignKey('Lection', on_delete=models.CASCADE, related_name='lection', null=True,
                                verbose_name='Лекция')
    type = models.CharField(max_length=10, choices=TYPES)

    def __str__(self):
        return '%s' % self.question


class FinalTaskInFinalTest(models.Model):
    class Meta:
        verbose_name = "Задача для финального теста"
        verbose_name_plural = "Задачи для финального теста"

    question = models.TextField(verbose_name="Вопрос", default="")
    points = models.FloatField(verbose_name='Максимальное кол-во очков за задание', default=0)
    lection = models.ForeignKey('Lection', on_delete=models.CASCADE, related_name='course_final_task', null=True,
                                blank=True,
                                verbose_name='Выберите лекцию для финального теста*')
    photo = models.ImageField(upload_to=task_folder, null=True, blank=True,
                              verbose_name='Фото задания для финального теста*')
    answer_is_photo = models.BooleanField(default=False)

    def __str__(self):
        return self.question + ' [Final]'


class Answers(models.Model):
    creater = models.ForeignKey(Student, related_name='answer_student', on_delete=models.CASCADE, null=False,
                                verbose_name='Создатель ответа')
    answer_text = models.TextField(verbose_name='Текст ответа', null=False, blank=False)
    is_right = models.BooleanField(verbose_name="Правильный ?", null=False, blank=False)

    def __str__(self):
        return '%s %s' % (self.answer_text, '[Верный]' if self.is_right else '[Не верный]')

    class Meta:
        verbose_name = 'Ответ'
        db_table = 'answers'
        verbose_name_plural = 'Ответы'
        unique_together = (('creater', 'answer_text', 'is_right'),)


class StudentAnswers(models.Model):
    student = models.ForeignKey(Student, null=False, verbose_name='Студент')
    task = models.ForeignKey('Tasks', related_name="task", null=False, verbose_name='Задача')
    given_answers = models.ManyToManyField('Answers', related_name='given_answers', null=False,
                                           verbose_name='Ответ студента')
    text_answer = models.TextField(null=True)

    class Meta:
        db_table = 'student_answers'
        unique_together = (('student', 'task'),)
        verbose_name = 'Ответ студента'
        verbose_name_plural = 'Ответы студентов'

    def are_correct(self):
        if len(self.given_answers.all()) != len(self.task.answers.filter(is_right=True).all()):
            return False
        given_answers = self.given_answers.order_by('answer_text').only('answer_text')
        task_right_answers = self.given_answers.order_by('answer_text').only('answer_text')
        for i in range(0, len(given_answers)):
            if given_answers[i] != task_right_answers[i]:
                return False
        return True


class StudentFinalTestAnswers(models.Model):
    student = models.ForeignKey(Student, null=False, verbose_name="Клиент")
    task = models.ForeignKey(FinalTaskInFinalTest, related_name="finaltask_answer", verbose_name="Финальная задача")
    answer = models.TextField(null=True, blank=True, verbose_name="Ответ студента:")
    photo = models.ImageField(upload_to=user_task_folder, null=True, blank=True, verbose_name="Ответ студента:")
    is_solved = models.BooleanField(default=False, )

    def image_tag(self):
        return """<img src="http://res.cloudinary.com/educationsphere/image/upload/v1494417475/media/%s"/>""" % self.photo.name

    image_tag.short_description = 'Image'
    image_tag.allow_tags = True

    def __str__(self):
        return self.task.question + " : " + self.student.user.username

    class Meta:
        db_table = 'student_final_answers'
        unique_together = (('student', 'task'),)
        verbose_name = 'Ответ студента на финальную задачу'
        verbose_name_plural = 'Ответы студентов на финальные задачи'


class Review(models.Model):
    user = models.ForeignKey(Student, on_delete=models.CASCADE, verbose_name='Студент')
    course = models.ForeignKey('Course', on_delete=models.CASCADE, verbose_name='Курс', null=True, blank=True)
    text = models.TextField(verbose_name='Текст комментария', max_length=3000, blank=True, null=False)
    date = models.DateTimeField(default=datetime.datetime.today, null=True, verbose_name='Дата комментария')

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзыв'

    def __str__(self):
        return '%s' % self.text


class FavoriteCourses(models.Model):
    course = models.ForeignKey('Course', on_delete=models.CASCADE, null=False, blank=False,
                               related_name='favorite_course',
                               default=None, verbose_name='Курс', unique=True)

    class Meta:
        verbose_name = 'Избранный курс'
        verbose_name_plural = 'Избранные курсы'

    def __str__(self):
        return '%s' % self.course.name


class FavoriteReview(models.Model):
    review = models.OneToOneField('Review', on_delete=models.CASCADE, null=False, blank=False,
                                  related_name='favorite_review',
                                  default=None, verbose_name='Отзыв', unique=True)

    class Meta:
        verbose_name = 'Лучший отзыв'
        verbose_name_plural = 'Лучшие отзывы'

    def __str__(self):
        return '%s' % self.review.user.user.first_name


class StudentLection(models.Model):
    student = models.ForeignKey(Student, null=False)
    lection = models.ForeignKey(Lection, null=False)
    points = models.IntegerField(default=0)

    class Meta:
        db_table = 'student_lections'
        unique_together = (('student', 'lection'),)


class Comments(models.Model):
    student = models.ForeignKey(Student, related_name='comment_student', on_delete=models.CASCADE,
                                verbose_name='Студент')
    lection = models.ForeignKey('Lection', related_name='comment_lection', on_delete=models.CASCADE,
                                verbose_name='Лекция')
    message = models.TextField(default='', max_length=500, verbose_name='Текст сообщения')
    pub_date = models.DateTimeField(default=datetime.datetime.now, verbose_name='Дата сообщения')
    like = models.IntegerField(default=0)

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
        db_table = 'comments'

    def __str__(self):
        return '%s' % self.message

    def isRep(self):
        return False


class RepsComment(models.Model):
    class Meta:
        db_table = 'reply_admin_comments'

    comment = models.ForeignKey('Comments', unique=False, related_name='rep_com', on_delete=models.CASCADE)
    student = models.ForeignKey(Student, related_name='student_rep', on_delete=models.CASCADE)
    lection = models.ForeignKey('Lection', related_name='rep_lection', on_delete=models.CASCADE,
                                verbose_name='Лекция')
    message = models.TextField(default='', max_length=500, verbose_name='Текст сообщения')
    pub_date = models.DateTimeField(default=datetime.datetime.now, verbose_name='Дата сообщения')
    like = models.IntegerField(default=0)

    def isRep(self):
        return True


class Certificates(models.Model):
    student = models.ForeignKey(Student, related_name="student_certs", verbose_name="Студент", null=False, blank=False)
    course = models.ForeignKey(Course, related_name="course_certs", verbose_name="Курс", null=False, blank=False)
    image = models.ImageField(upload_to=certificate_folder, null=False, blank=False,
                              verbose_name="Документ сертификата")

    class Meta:
        db_table = 'certificates'
        unique_together = (('student', 'course'),)
        verbose_name = 'Сертификат'
        verbose_name_plural = 'Сертификаты'
