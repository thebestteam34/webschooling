import json
import random
from _operator import attrgetter
from itertools import chain
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.mail import send_mail
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import Http404
from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.http.response import HttpResponseRedirect as Redirect, HttpResponseRedirect, HttpResponseForbidden, \
    HttpResponse, JsonResponse
from django.template import RequestContext
from django.template.context_processors import csrf
from django.urls import reverse_lazy
from django.utils import dateformat
from auth.validators.decorator import login_required_24hours
from courses.forms import ReviewForm, SupportForm, UploadFileForm
from courses.models import *
from courses.validators.decorator import *
from courses.validators.validator import user_must_subscribe


def main(request):
    args = {}
    # args['favorite_courses'] = FavoriteCourses.objects.all()[:3]
    args['reviews'] = Review.objects.filter(course=None)
    args['selected_courses'] = FavoriteCourses.objects.all()
    return render(request, 'main.html', args)


def list_courses(request):
    args = {'courses': Course.objects.all()}
    pagination = Paginator(args['courses'], 6)
    page = request.GET.get('page')
    args['directions'] = directions_of_training
    try:
        courses = pagination.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        courses = pagination.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        courses = pagination.page(pagination.num_pages)
    args['courses'] = courses
    return render(request, 'courses/list_courses.html', args)


def show_course(request, course_id):
    if request.method == 'POST':
        if not request.user.is_authenticated():
            return HttpResponseRedirect('/auth/login/?next=/courses/%s' % course_id)

        if request.POST['form'] == 'subscribe':
            if not Subscribe.objects.filter(student=request.user.student_user, course_id=course_id).exists():
                if Lection.objects.filter(course_id=course_id).exists():
                    lection = Lection.objects.filter(course=course_id)
                    course = get_object_or_404(Course, pk=course_id)
                    if course.date_begining.timestamp() < datetime.datetime.now().timestamp() and course.date_ending.timestamp() > datetime.datetime.now().timestamp():
                        Subscribe(student=request.user.student_user, course_id=course_id,
                                  lection_status=lection.get(order=1)).save()
                    else:
                        raise Http404(
                            'Невозможно записаться на курс, так как вы не успели записаться до даты окончания записи.')
                else:
                    raise Http404("У курса нет лекций")
            else:
                Subscribe.objects.filter(student=request.user.student_user, course_id=course_id).delete()
            return HttpResponseRedirect(reverse('courses:one', kwargs={'course_id': course_id}))
    else:
        show_final_test = False
        has_previous = hasattr(Course.objects.get(id=course_id), 'course_next')
        certificate = False
        button = True
        if not request.user.is_authenticated and has_previous:
            button = False
        subscribe_exists = False
        not_80_percent = True
        if request.user.is_authenticated():
            student = request.user.student_user
            if Subscribe.objects.filter(student=student, course_id=course_id).exists():
                subscribe = Subscribe.objects.get(course_id=course_id, student=student)
                course = get_object_or_404(Course, id=course_id)
                not_80_percent = False if course.control_points * 0.8 <= subscribe.points else True
                if Subscribe.objects.get(student=student,
                                         course_id=course_id).final_test_points >= Course.objects.get(
                    id=course_id).control_final_test_points:
                    certificate = True
                if Subscribe.objects.get(student=student, course_id=course_id).points >= Course.objects.get(
                        id=course_id).control_points:
                    show_final_test = True
            if has_previous:
                course = Course.objects.get(id=course_id).course_next
                if Subscribe.objects.filter(student=student, course=course).exists():
                    if Subscribe.objects.get(student=student,
                                             course=course).final_test_points < course.control_final_test_points:
                        button = False
                else:
                    button = False
            subscribe_exists = Subscribe.objects.filter(student=student, course_id=course_id).exists()

        course = get_object_or_404(Course, id=course_id)

        lection = None
        try:
            lection = Lection.objects.filter(course_id=course.id).exclude(is_final=True).order_by('order')
        except Exception:
            raise Http404('Course doesn\'t have lections')

        review = show_review(request, course_id)

        return render(request, 'courses/show_course.html', {'course': course,
                                                            'subscribed': subscribe_exists,
                                                            'lections': lection,
                                                            'reviews': review['reviews'],
                                                            'errors': review['errors'],
                                                            'count': len(review['reviews']),
                                                            'button': button,
                                                            'certificate': certificate,
                                                            "not_80_percent": not_80_percent,
                                                            'show_test': show_final_test
                                                            })


@login_required_24hours(login_url=reverse_lazy('auth:logout'))
@not_subscribed_fuckoff(user_must_subscribe)
def list_lections(request, course_id):
    args = {}
    if request.method == 'GET':
        course = get_object_or_404(Course, id=course_id)
        lections = Lection.objects.filter(course=course).exclude(is_final=True).order_by('order')
        args['course'] = course
        args['lections'] = lections
        return render(request, 'courses/list_lections.html', args)
    else:
        pass


@login_required_24hours(login_url=reverse_lazy('auth:logout'))
@not_subscribed_fuckoff(user_must_subscribe)
def show_lection(request, course_id, lection_id):
    args = {}
    if request.method == "GET":
        student_exists = Student.objects.filter(user=request.user.id).exists()
        if not student_exists:
            raise Http404('Вход доступен только студентам!')
        else:
            tasks = Tasks.objects.filter(lection_id=lection_id)
            student = Student.objects.get(user=request.user.id)
            course = get_object_or_404(Course, pk=course_id)
            args['course'] = course
            lections = None
            comments = None
            replyes = None
            try:
                lection = Lection.objects.filter(course_id=course_id).get(id=lection_id)
                comments = Comments.objects.filter(lection_id=lection_id).order_by('-pub_date')
                replyes = RepsComment.objects.filter(lection_id=lection_id)
            except:
                raise Http404("This resource is not available.")
            args['tasks'] = tasks
            args['lection'] = lection
            args['comments'] = comments
            args['replyes'] = replyes
            args['objects'] = sorted(chain(comments, replyes), key=attrgetter('pub_date'), reverse=True)
            # Check for subscribed user to access in lection
            # if not Subscribe.objects.filter(course=course).filter(student=student).filter(
            #         lection_status__order__gte=lection_id).exists():
            #     raise Http404("Access denied!")
            # else:
            return render(request, 'courses/show_lection.html', args)
    elif request.method == 'POST':
        args.update(csrf(request))
        if (request.POST.get('superuser') == 'aaa'):
            if len(request.POST['comment_message']) > 0:
                comment = Comments()
                comment.message = request.POST['comment_message']
                comment.student = request.user.student_user
                comment.lection = get_object_or_404(Lection, pk=int(lection_id))
                comment.save()
                data = {
                    'comment_user_photo': comment.student.photo.url,
                    'comment_username': comment.student.user.first_name + ' ' + comment.student.user.first_name,
                    'comments_pub_date': dateformat.format(comment.pub_date, settings.DATE_FORMAT),
                    'comment_like': comment.like,
                    'comment_messaage': comment.message
                }
                return JsonResponse(data)
            else:
                return JsonResponse({'error': "Нельзя оставлять пустой комментарий"})
        else:
            try:
                reply = RepsComment()
                message_rep = request.POST['comment_message']
                message_rep = "<p><b>" + message_rep
                message_rep_massive = message_rep.split(':')
                message_rep_massive[1] = "</b></p>" + message_rep_massive[1]
                message_rep = ""
                for mess in message_rep_massive:
                    message_rep += mess
                reply.message = message_rep
                reply.student = request.user.student_user
                reply.lection = get_object_or_404(Lection, pk=int(lection_id))
                reply.comment = get_object_or_404(Comments, pk=int(request.POST['replyest']))
                reply.save()
            except:
                comment = Comments()
                comment.message = request.POST['comment_message']
                comment.student = request.user.student_user
                comment.lection = get_object_or_404(Lection, pk=int(lection_id))
                comment.save()
            return Redirect(reverse('courses:lection', kwargs={'course_id': course_id, 'lection_id': lection_id}))


@login_required_24hours(login_url=reverse_lazy('auth:logout'))
@not_subscribed_fuckoff(user_must_subscribe)
def show_tasks(request, course_id, lection_id):
    student_exists = Student.objects.filter(user=request.user.id).exists()
    if not student_exists:
        raise Http404('Вход доступен только студентам!')
    if Lection.objects.get(id=lection_id).is_final:
        return HttpResponseRedirect(reverse('courses:test', kwargs={'course_id': course_id}))
    if StudentLection.objects.filter(student=request.user.student_user, lection_id=lection_id).exists():
        tasks = Tasks.objects.filter(lection_id=lection_id)
        answers = {}
        given_answers = {}
        right_answers = {}
        right = {}
        points = StudentLection.objects.get(student=request.user.student_user, lection_id=lection_id).points
        for task in tasks:
            string_answers_for_task = []
            for answer in task.answers.all().order_by('answer_text'):
                string_answers_for_task.append(answer.answer_text)
            answers[task.id] = string_answers_for_task
            if task.type == 'text':
                given_answers[task.id] = StudentAnswers.objects.get(student=request.user.student_user,
                                                                    task=task).text_answer
                right_answers[task.id] = Tasks.objects.get(id=task.id).answers.filter(is_right=True)[
                                         :1].get().answer_text
            else:
                string_given_answers = []
                for answer in StudentAnswers.objects.get(student=request.user.student_user,
                                                         task=task).given_answers.all():
                    string_given_answers.append(answer.answer_text)
                given_answers[task.id] = string_given_answers
                string_right_answers = []
                for answer in task.answers.filter(is_right=True):
                    string_right_answers.append(answer.answer_text)
                right_answers[task.id] = string_right_answers
        return render(request, 'courses/tasks_results.html',
                      {'answers': answers, 'tasks': list(tasks), 'student': request.user.student_user,
                       'given_answers': given_answers, 'right_answers': right_answers, 'points': points,
                       'final_test': False, 'right': right})

    if request.method == 'GET':
        tasks = Tasks.objects.filter(lection_id=lection_id)
        if tasks:
            # random.shuffle(tasks)
            pass
        else:
            raise Http404("У лекции нет задач")
        answers = {}
        for task in tasks:
            string_answers = []
            for answer in task.answers.all():
                string_answers.append(answer.answer_text)
            answers[task.id] = string_answers
        return render(request, 'courses/list_tasks.html',
                      {'answers': answers, 'tasks': list(tasks), 'course_id': course_id, 'lection_id': lection_id})
    else:
        points = 0
        tasks = Tasks.objects.filter(lection_id=lection_id)
        for task in tasks:
            answers = []
            if task.type == 'text':
                answers = list(map(str.strip, request.POST.getlist(str(task.id))))
                student_answers = StudentAnswers.objects.create(task=task, student=request.user.student_user,
                                                                text_answer=answers[0])
            else:
                answers = request.POST.getlist(str(task.id))
                student_answers = StudentAnswers.objects.create(task=task, student=request.user.student_user)
                for given_answer in answers:
                    answer = task.answers.get(answer_text=given_answer)
                    student_answers.given_answers.add(answer)
            right_answers = []
            count = task.answers.filter(is_right=True).count()
            points_for_task = task.points
            part_of_points_for_task = int(round(points_for_task / count))
            for answer in task.answers.filter(is_right=True).order_by('answer_text'):
                right_answers.append(answer.answer_text)
            s = Subscribe.objects.get(student=request.user.student_user, course_id=course_id)
            student_points_for_task = 0
            if sorted(answers) == right_answers:
                s.points += int(points_for_task)
                points += int(points_for_task)
                s.save()
            else:
                part_of_points_for_task = int(round(points_for_task / count))
                for answer in answers:
                    if answer in right_answers:
                        student_points_for_task += part_of_points_for_task
                    else:
                        student_points_for_task -= part_of_points_for_task
            print(student_points_for_task)
            if not student_points_for_task < 0:
                points += student_points_for_task
                s.points += student_points_for_task
                s.save()

        StudentLection.objects.create(student=request.user.student_user, lection_id=lection_id, points=points)
        return HttpResponseRedirect(
            reverse('courses:lection_task', kwargs={'course_id': course_id, 'lection_id': lection_id}))


@login_required_24hours(login_url=reverse_lazy('auth:logout'))
@not_subscribed_fuckoff(user_must_subscribe)
def show_test(request, course_id):
    student_exists = Student.objects.filter(user=request.user.id).exists()
    if not student_exists:
        raise Http404('Вход доступен только студентам!')
    if Subscribe.objects.get(student=request.user.student_user, course_id=course_id).points < Course.objects.get(
            id=course_id).control_points:
        return HttpResponseForbidden()

    if Subscribe.objects.get(student=request.user.student_user, course_id=course_id).final_test_points > -1:
        final_lection = Lection.objects.filter(course_id=course_id).filter(is_final=True)
        tasks = Tasks.objects.filter(lection=final_lection)
        answers = {}
        given_answers = {}
        right_answers = {}
        points = Subscribe.objects.get(student=request.user.student_user, course_id=course_id).final_test_points
        for task in tasks:
            string_answers_for_task = []
            for answer in task.answers.all().order_by('answer_text'):
                string_answers_for_task.append(answer.answer_text)
            answers[task.id] = string_answers_for_task
            if task.type == 'text':
                given_answers[task.id] = StudentAnswers.objects.get(student=request.user.student_user,
                                                                    task=task).text_answer
                right_answers[task.id] = Tasks.objects.get(id=task.id).answers.filter(is_right=True)[
                                         :1].get().answer_text
            else:
                string_given_answers = []
                for answer in StudentAnswers.objects.get(student=request.user.student_user,
                                                         task=task).given_answers.all():
                    string_given_answers.append(answer.answer_text)
                given_answers[task.id] = string_given_answers
                string_right_answers = []
                for answer in task.answers.filter(is_right=True):
                    string_right_answers.append(answer.answer_text)
                right_answers[task.id] = string_right_answers
        return render(request, 'courses/tasks_results.html',
                      {'answers': answers, 'tasks': list(tasks), 'student': request.user.student_user,
                       'given_answers': given_answers, 'right_answers': right_answers, 'points': points,
                       'final_test': True})

    if request.method == 'GET':
        final_lection = Lection.objects.get(course_id=course_id, is_final=True)
        simple_tasks = Tasks.objects.filter(lection=final_lection)
        final_tasks = FinalTaskInFinalTest.objects.filter(lection=final_lection)
        tasks = chain(simple_tasks, final_tasks)
        answers = {}
        upload_file_form = UploadFileForm()
        for task in simple_tasks:
            string_answers = []
            for answer in task.answers.all():
                string_answers.append(answer.answer_text)
            answers[task.id] = string_answers
        for task in final_tasks:
            answers['photo' + str(task.id)] = "final"
        return render(request, 'courses/list_tasks.html',
                      {'answers': answers, 'tasks': list(tasks), 'lection_id': final_lection.id,
                       'course_id': course_id, 'f': upload_file_form})
    else:
        final_lection = Lection.objects.filter(course_id=course_id).filter(is_final=True)
        simple_tasks = Tasks.objects.filter(lection=final_lection)
        final_tasks = FinalTaskInFinalTest.objects.filter(lection=final_lection)
        tasks = chain(simple_tasks, final_tasks)
        s = Subscribe.objects.get(student=request.user.student_user, course_id=course_id)
        s.final_test_points = 0
        s.save()
        for task in simple_tasks:
            answers = []
            if task.type == 'text':
                answers = list(map(str.strip, request.POST.getlist(str(task.id))))
                student_answers = StudentAnswers.objects.create(task=task, student=request.user.student_user,
                                                                text_answer=answers[0])
            else:
                answers = request.POST.getlist(str(task.id))
                student_answers = StudentAnswers.objects.create(task=task, student=request.user.student_user)
                for given_answer in answers:
                    answer = task.answers.get(answer_text=given_answer)
                    student_answers.given_answers.add(answer)
            right_answers = []
            count = task.answers.filter(is_right=True).count()
            points_for_task = task.points
            part_of_points_for_task = int(round(points_for_task / count))
            for answer in task.answers.filter(is_right=True).order_by('answer_text'):
                right_answers.append(answer.answer_text)
            s = Subscribe.objects.get(student=request.user.student_user, course_id=course_id)
            student_points_for_task = 0
            if sorted(answers) == right_answers:
                s.final_test_points += int(points_for_task)
                s.save()
            else:
                part_of_points_for_task = int(round(points_for_task / count))
                for answer in answers:
                    if answer in right_answers:
                        student_points_for_task += part_of_points_for_task
                    else:
                        student_points_for_task -= part_of_points_for_task
            print(student_points_for_task)
            if not student_points_for_task < 0:
                s.final_test_points += student_points_for_task
                s.save()
        for task in final_tasks:
            key = int(str('photo' + str(task.id))[5:])
            if not task.answer_is_photo:
                answer = request.POST.get('photo' + str(task.id))
                answer = str(answer).strip()
                StudentFinalTestAnswers.objects.create(student=request.user.student_user, task_id=key, answer=answer)
            else:
                file = request.FILES['file']
                path = 'media/tasks_img/%s/%s.jpeg' % (request.user.username, task.question)
                path_for_db = 'tasks_img/%s/%s.jpeg' % (request.user.username, task.question)
                directory = 'media/tasks_img/%s' % (request.user.username)
                if not os.path.exists(directory):
                    os.makedirs(directory)
                f = open(path, 'w')
                f.close()
                # if not os.path.exists(directory):
                #    os.makedirs(directory)
                with open(path, 'wb+') as destination:
                    for chunk in file.chunks():
                        destination.write(chunk)
                StudentFinalTestAnswers.objects.create(student=request.user.student_user, task_id=key,
                                                       photo=path_for_db)

        teachers_exist = hasattr(Course.objects.get(id=course_id).creater, 'company_teachers')
        if teachers_exist:
            teachers = Course.objects.get(id=course_id).creater.company_teachers.teachers.all()
            teacher = teachers[random.randint(0, teachers.count() - 1)]
            course_name = Course.objects.get(id=course_id).name
            subject = "Answers from %s for Final Test of %s" % (request.user.username, course_name)
            message = 'You have to check answers of %s for Final Test of %s in your account.' % (
                request.user.username, course_name)
            send_mail(subject, message, request.user.username, [teacher.user.username],
                      fail_silently=False)
        return HttpResponseRedirect(
            reverse('courses:test', kwargs={'course_id': course_id}))


def course_search(request):
    args = {}
    search = request.GET['search_text']
    direction = request.GET['direction']
    if direction == 'ALL':
        if search == "":
            args['courses'] = Course.objects.all()
        else:
            args['courses'] = Course.objects.filter(name__icontains=search)
    else:
        args['courses'] = Course.objects.filter(name__icontains=search, direction=direction)
    pagination = Paginator(args['courses'], 6)
    page = request.GET.get('page')
    try:
        courses = pagination.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        courses = pagination.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        courses = pagination.page(pagination.num_pages)
    args['courses'] = courses
    args['directions'] = directions_of_training
    args['direction'] = direction
    args['search'] = search
    return render(request, 'courses/list_courses.html', args)


def show_review(request, course_id):
    reviews = Review.objects.filter(course_id=course_id)
    return {'reviews': reviews.order_by('-date'), 'errors': request.session.get('error', None)}


@login_required_24hours(login_url=reverse_lazy('auth:logout'))
@not_subscribed_fuckoff(user_must_subscribe)
def reviews(request, course_id):
    error = []
    student = get_object_or_404(Student, user=request.user)
    course = Course.objects.get(id=course_id)
    subscribe = Subscribe.objects.get(course_id=course_id, student=student)
    if course.control_points * 0.8 <= subscribe.points:
        if request.method == 'POST':
            if (request.POST['comment']):
                review = Review(user=student, course_id=course_id, text=request.POST['comment'])
                review.save()
                reviews_count = Review.objects.filter(course_id=course_id).count()

                data = {
                    "message": review.text,
                    "photo": review.user.photo.url,
                    "name": review.user.user.first_name + " " + review.user.user.last_name,
                    "date": dateformat.format(review.date, settings.DATE_FORMAT),
                    "count": reviews_count
                }

                return JsonResponse(data)
            else:
                return JsonResponse({'error': 'Нельзя оставить пустой отзыв'})


@login_required_24hours(login_url=reverse_lazy('auth:logout'))
@not_subscribed_fuckoff(user_must_subscribe)
def support(request, course_id):
    args = {}
    args['course_id'] = course_id
    course = get_object_or_404(Course, pk=course_id)
    args['form'] = SupportForm()
    if request.method == 'POST':
        supForm = SupportForm(request.POST)
        if supForm.is_valid():
            user = User.objects.get(student_user__company_user__course=course_id)
            supForm.sendmail(request.user.first_name, user.username, course_name=course.name)
            return HttpResponse("The mail was sended!")
        else:
            args['form'] = supForm
            messages.add_message(request, messages.ERROR, 'Please enter the correct data')
            return render(request, 'support.html', args)
    else:
        return render(request, 'support.html', args)
