from string import Template

import cloudinary
import cloudinary.uploader
import cloudinary.api
from django import forms
from django.contrib.auth.models import User
from django.contrib.gis import admin
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.utils.safestring import mark_safe

from auth.models import Student
from courses.models import Review, Lection, Answers, Course, Company, TeachersGroup, StudentFinalTestAnswers


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = ['text']


class SupportForm(forms.Form):
    subject = forms.CharField(max_length=300)
    message = forms.CharField(widget=forms.Textarea, max_length=4000)

    def sendmail(self, username, email, course_name):
        subject = self.cleaned_data['subject']
        message = self.cleaned_data['message']
        send_mail(subject, str(course_name) + ':  ' + message, username, [email],
                  fail_silently=False)


class LectionAdminForm(forms.ModelForm):
    class Meta:
        model = Lection
        widgets = {
            'youtube_url': admin.widgets.Textarea,
        }
        fields = '__all__'

    def is_valid(self):
        valid = super(LectionAdminForm, self).is_valid()
        if not valid:
            return valid
        if self.cleaned_data['is_final']:
            for lection in self.cleaned_data['course'].lec_course.all():
                if lection.is_final:
                    self.add_error('is_final', 'Финальная лекция для этого курса уже существует!')
                    return False
        return True


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answers
        fields = ('answer_text', 'is_right')

    def saving(self, user):
        return super(AnswerForm, self).save()

    def saveOverrided(self, user):
        return self.saving(user)


class TeachersGroupForm(forms.ModelForm):
    class Meta:
        fields = ['company', 'name', 'teachers', ]


class PictureWidget(forms.widgets.Widget):
    def render(self, name, value, attrs=None):
        html = Template("""<img src="$link"/>""")
        return mark_safe(html.substitute(link=value))


class FinalTestForm(forms.ModelForm):
    class Meta:
        model = StudentFinalTestAnswers
        fields = ['student', 'answer', 'image_question', "question", "mark", 'photo', "points"]

    question = forms.CharField(widget=forms.Textarea(attrs={'rows': 5, 'cols': 100}), )
    image_question = forms.ImageField(widget=PictureWidget)
    mark = forms.FloatField()
    points = forms.FloatField()

    def empty(self):
        pass;

    points.validate = empty
    mark.validate = empty
    image_question.validate = empty
    question.validate = empty

    def is_valid(self):
        valid = super(FinalTestForm, self).is_valid()
        if not valid:
            return valid
        if float(self.data['mark']) > float(self.data['points']):
            self.add_error('mark', 'Оценка не может превышать Максимальную оценку за задание!')
            return False
        if float(self.data['mark']) < 0:
            self.add_error('mark', 'Оценка не может быть меньше 0!')
            return False
        return True

    # TODO show task in form
    def __init__(self, *args, **kwargs):
        super(FinalTestForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            # self.fields['student'].widget.attrs['readonly'] = True
            # self.fields['image_question'].widget.attrs['readonly'] = True
            self.fields['question'].widget.attrs['readonly'] = True
            self.fields['points'].widget.attrs['readonly'] = True
            self.fields['question'].label = "Вопрос"
            self.fields['image_question'].label = "Задание"
            self.fields['mark'].label = "Ваша оценка"
            self.fields['points'].label = "Максимальная оценка за задание"
            # self.fields['answer'].widget.attrs['readonly'] = True

    def clean_student(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return instance.student
        else:
            return self.cleaned_data['student']

    def clean_task(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return instance.task
        else:
            return self.cleaned_data['task']

    def clean_photo(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return instance.photo
        else:
            return self.cleaned_data['photo']

    def clean_answer(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return instance.answer
        else:
            return self.cleaned_data['answer']


class CourseFormAdmin(forms.ModelForm):
    class Meta:
        model = Course
        fields = '__all__'

    def saveOverrided(self, user, course):
        if Course.objects.filter(id=course.id).exists():
            course = Course.objects.get(id=course.id)
        course.name = self.cleaned_data['name']
        course.about = self.cleaned_data['about']
        if ('creater' in self.fields):
            course.creater = self.cleaned_data['creater']
        else:
            course.creater = self.company
        course.image = self.cleaned_data['image']
        course.direction = self.cleaned_data['direction']
        course.date_begining = self.cleaned_data['date_begining']
        course.date_ending = self.cleaned_data['date_ending']
        course.control_points = self.cleaned_data['control_points']
        course.control_final_test_points = self.cleaned_data['control_final_test_points']
        course.save()
        s = str(course.image).split('.')
        e = str()
        for i in range(0, len(s) - 1):
            e += s[i] + '.'
        filename = e[0:-1]
        print(filename)
        cloudinary.uploader.upload(course.image, public_id='media/' + filename)
        return course


class UploadFileForm(forms.Form):
    file = forms.FileField()
